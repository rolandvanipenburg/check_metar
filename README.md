# NAME

check\_metar - check METAR data

# USAGE

**check\_metar** **-u** _url_ **-s** _site_ **-p** _property_ **-w** _warning_ **-c** _critical_

# DESCRIPTION

**check\_metar** is a Nagios plugin that can check meteorological information in
METAR format

# REQUIRED ARGUMENTS

- **-u, --url**

    URL to get a METAR from. If the ICAO of the site is covered by `weather.gov`
    or `knmi.nl` the URL can be shortened to be only that domain. This is to avoid
    having to specify the site's ICAO code in both the URL and as site parameter.

- **-s, --site**

    The site as [ICAO](https://metacpan.org/pod/Geo%3A%3AICAO) code for the METAR

- **-p, --property**

    The property to get the value for. Must be one of:

    - alt, alt\_hpa, alt\_inhg

        Altimeter setting in the standard inches of mercury or the explicit variants
        for hectopascal and inches of mercury.

    - pressure

        Pressure in Pascal. 

    - temp\_c, temp\_f

        Temperature in Celsius or Fahrenheit.

    - dew\_c, dew\_f

        Dew point in Celsius or Fahrenheit.

    - visibility, visibility\_km, visibility\_sm

        Visibility in meters, kilometers or statute miles.

    - ceiling

        The ceiling – based on cloud level or vertical visibility – in feet.

    - flight\_rule

        The flight rule based on ceiling and visibility as 0 for low IFR, 1 for
        IFR, 2 for marginal VFR and 3 for VFR.

    - wind\_kts, wind\_mph

        Wind speed in knots or miles per hour.

    - wind\_gust\_kts, wind\_gust\_mph

        Wind gust speed in knots or miles per hour.

    - wind\_dir\_deg, wind\_dir\_abb, wind\_dir\_eng

        Wind direction in degrees, as English abbreviation – like NW – or English –
        like Northwest.

    - wind\_var, wind\_var\_1, wind\_var\_2

        Whether the wind is varying and the lowest and highest wind directions of the
        varying wind in degrees.

    - blowing, drizzle, dust, dust\_sand\_whirls, dust\_storm, fog, fog\_banks,
    freezing, funnel\_cloud, hail, haze, ice\_crystals, ice\_pellets, mist, partial,
    patches, rain, sand, sand\_storm, shallow, shower, small\_hail\_snow\_pellets,
    smoke, snow, snow\_grains, spray, squalls, thunderstorm, unknown\_precip,
    volcanic\_ash

        Weather types and other phenomena as 0 for not observed, 1 when in a light
        condition, 2 for a normal condition and 3 for heavy.

# OPTIONS

- **--ssl**

    The `SSL` version as used by [IO::Socket::SSL](https://metacpan.org/pod/IO%3A%3ASocket%3A%3ASSL) as `SSL_version` option. This
    is sometimes needed when a server throws an error when trying to connect using
    a version higher than `TLSv12`.

- **-m, --max\_age**

    The maximum cache age in seconds used for the retrieved data. This is meant to
    be used with a small value so checks for multiple properties in the same
    monitoring cycle can share a request. It doesn't make sense to use values that
    exceed the monitoring interval duration, then the monitoring interval should
    just be increased.

- **-?, -h, --help**

    Show help

- **-v, --verbose**

    Be more verbose

- **-V, --version**

    Show version and license

# DIAGNOSTICS

- _Could not retrieve page '%s' to get data: Exception reason_
(E) The page containing the data could not be retrieved from the URL
- _Could not get data from page '%s' for site '%s'_
(E) The page containing the data could be retrieved from the URL but the
requested data was not found in that object

# EXAMPLES

`check_metar -u
https://www.knmi.nl/nederland-nu/luchtvaart/vliegveldwaarnemingen --ssl TLSv1 -s EHAM -p
temp_c -w25: -c30:`

`check_metar -u knmi.nl -s EHAM -p temp_c -w25: -c30:`

`check_metar -u weather.gov -s KD55 -p temp_f -w50: -c80:`

# CONFIGURATION

The environment variable _HTTP\_TINY\_CACHE\_MAX\_AGE_ is used to set the default
caching period for requests to 60 seconds if it wasn't set already, or to the
value given by the _max\_age_ option.

# DEPENDENCIES

Perl 5.20.0, [Monitoring::Plugin](https://metacpan.org/pod/Monitoring%3A%3APlugin), [Geo::METAR::Deduced](https://metacpan.org/pod/Geo%3A%3AMETAR%3A%3ADeduced),
[HTTP::Tiny::Cache](https://metacpan.org/pod/HTTP%3A%3ATiny%3A%3ACache), [Readonly](https://metacpan.org/pod/Readonly), [Net::SSLeay](https://metacpan.org/pod/Net%3A%3ASSLeay), <IO::Socket::SSL>

These dependencies can be installed by running the command
`sudo cpanm Monitoring::Plugin Geo::METAR::Deduced HTTP::Tiny::Cache Readonly
Net::SSLeay IO::Socket::SSL`

# EXIT STATUS

The exit status is handled by the monitoring interface.

# INCOMPATIBILITIES

There are no known incompatibilities but since the data is coming from a source
without a predefined format there's always a chance that data turns out to be
incompatible.

# BUGS AND LIMITATIONS

- The data source which contains the METAR must include the METAR indicator
at the beginning of the message.
- Runway specific data that might be used in METAR is not supported

Please report any bugs or feature requests at [Issues for
Bitbucket](https://bitbucket.org/rolandvanipenburg/check_metar/issues).

# AUTHOR

Roland van Ipenburg, <roland@rolandvanipenburg.com>

# LICENSE AND COPYRIGHT

Copyright 2020-2024 by Roland van Ipenburg
This program is free software; you can redistribute it and/or modify
it under the GNU General Public License v3.0.

# DISCLAIMER OF WARRANTY

BECAUSE THIS SOFTWARE IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
FOR THE SOFTWARE, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN
OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES
PROVIDE THE SOFTWARE "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE
ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE SOFTWARE IS WITH
YOU. SHOULD THE SOFTWARE PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL
NECESSARY SERVICING, REPAIR, OR CORRECTION.

IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR
REDISTRIBUTE THE SOFTWARE AS PERMITTED BY THE ABOVE LICENSE, BE
LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL,
OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE
THE SOFTWARE (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING
RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A
FAILURE OF THE SOFTWARE TO OPERATE WITH ANY OTHER SOFTWARE), EVEN IF
SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGES.
